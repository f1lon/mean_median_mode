import {Stats} from './code';

const stat1 = new Stats([1, 2, 3, 4, 4, 5, 5]);
const stat2 = new Stats([4, 1, 4, 3, 3, 1, 2, 2]);

describe('Round', () => {
   it('should round to 2 decimal places', () => {
       expect(Stats.round(1.235)).toBe(1.24);
       expect(Stats.round(2.2)).toBe(2.2);
   })
});

describe('Mean', () => {
    it('Should implement mean', () => {
        expect(Stats.round(stat1.mean())).toBe(3.43);
        expect(Stats.round(stat2.mean())).toBe(2.5);
    });
});

// https://en.wikipedia.org/wiki/Median
// [1, 2, 3], [1, 2, 3, 4, 5, 6]
//     ^             ^  ^
describe('Median', () => {
    it('Should implement median', () => {
        expect(stat1.median()).toBe(4);
        expect(stat2.median()).toBe(2.5);
    });
});

// https://en.wikipedia.org/wiki/Mode_(statistics)
describe('Mode', () => {
    it('Should implement mode', () => {
        expect(stat1.mode()).toEqual([4, 5]);
        expect(stat2.mode()).toEqual([]);
    });
});
